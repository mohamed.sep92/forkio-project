### DAN.IT-Step-Project-Forkio

Responsive Landing Page

### Technologies Index

1. HTML5, CSS(SCSS), JS(ES6)
2. Node, NPM, Gulp

### Teamwork:

[Mohamed](https://gitlab.com/mohamed.sep92/forkio-project/-/tree/master)

### Gitlab pages:

[visit our gitlab page](https://mohamed.sep92.gitlab.io/forkio-project/)
